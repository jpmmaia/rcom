#include "Application.h"
#include "Emitter.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>

const char Application::s_C_DATA = 1;
const char Application::s_C_START = 2;
const char Application::s_C_END = 3;

const char Application::s_T_FILE_SIZE = 0;
const char Application::s_T_FILENAME = 1;

Application::Application(Application::connector_t connector)
{
	m_connector = connector;
	m_maxFragmentSize = 2048;
	m_maxPacketSize = 4 + m_maxFragmentSize;
	m_filename = 0;
}
Application::~Application()
{
	deleteFilename();
}

const char* Application::getFilename() const
{
	return m_filename;
}

void Application::setMaxFragmentSize(unsigned int maxFragmentSize)
{
	if(maxFragmentSize < 46)
		maxFragmentSize = 46;

	m_maxFragmentSize = maxFragmentSize;
	m_maxPacketSize = 4 + maxFragmentSize;
}
void Application::setMaxPacketSize(unsigned int maxPacketSize)
{
	if(maxPacketSize < 50)
		maxPacketSize = 50;

	m_maxPacketSize = maxPacketSize;
	m_maxFragmentSize = maxPacketSize - 4;
}

void Application::setFilename(const char* filename)
{	
	// Delete current filename if it exists:
	deleteFilename();
	
	// Calculate filename total size including '/0':
	unsigned int filenameSize = strlen(filename) + 1;
	
	// Allocate memory for filename container:
	m_filename = (char*) malloc(filenameSize);
	
	// Copy original filename to destination:
	memcpy(m_filename, filename, filenameSize);
}

void Application::deleteFilename()
{
	if(m_filename)
	{
		free(m_filename);
		m_filename = 0;
	}
}
