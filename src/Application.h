#ifndef __APPLICATION_H__
#define __APPLICATION_H__


class Application
{
public:
	enum connector_t
	{
		receptor = 0,
		emitter = 1
	};

public:
	Application(connector_t connector);
	virtual ~Application();
	
	const char* getFilename() const;

	void setMaxFragmentSize(unsigned int maxFragmentSize);
	void setMaxPacketSize(unsigned int maxPacketSize);
	virtual void setBaudrate(unsigned int baudrate) = 0;
	virtual void setTimeout(unsigned int timeout) = 0;

protected:
	void setFilename(const char* filename);
	
private:
	void deleteFilename();

protected:
	connector_t m_connector;
	const char* m_com;
	unsigned int m_maxFragmentSize;
	unsigned int m_maxPacketSize;

	int m_fd;
	int m_fileSize;

	static const char s_C_DATA;
	static const char s_C_START;
	static const char s_C_END;

	static const char s_T_FILE_SIZE;
	static const char s_T_FILENAME;

private:
	char* m_filename;
};

#endif
