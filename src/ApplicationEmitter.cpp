#include "ApplicationEmitter.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>

ApplicationEmitter::ApplicationEmitter() : Application(Application::emitter)
{
}

bool ApplicationEmitter::alopen(const char* com, const char* filename)
{
	// Set serial port:
	Application::m_com = com;

	// Calculate file size:
	fprintf(stderr, "2.1 - Calculating file size!\n");
	Application::m_fileSize = calculateFileSize(filename);
	
	// Set filename:
	fprintf(stderr, "2.2 - Setting filename!\n");
	Application::setFilename(filename);

	// Open connection with serial port:
	fprintf(stderr, "2.3 - Opening connection with device %s!\n", com);
	Application::m_fd = open(com, O_RDWR | O_NOCTTY);

	// Link layer open connection:
	if(!m_emitter.llopen(Application::m_fd))
		return false;

	return true;
}
void ApplicationEmitter::alclose()
{
	// Link layer close connection:
	m_emitter.llclose();

	// Close connection with device:
	fprintf(stderr, "2.8 - Closing connection with device %s!\n", m_com);
	close(Application::m_fd);
}

void ApplicationEmitter::alwrite()
{
	while(1) { // Used for time out breaks
			
		// Write start control packet:
		fprintf(stderr, "2.4 - Writting start control packet!\n");
		writeControlPacket(s_C_START);

		if (m_emitter.isTimedOut()) break;

		// Write file data:
		fprintf(stderr, "2.5 - Writting data\n");
		writeData();

		if (m_emitter.isTimedOut()) break;

		// Write end control packet:
		fprintf(stderr, "2.6 - Writting end control packet!\n");
		writeControlPacket(s_C_END);
	
		if (m_emitter.isTimedOut()) break;
	
		return;
	}
	
	fprintf(stderr, "2.7 - Connection Timed Out!\n");
}

void ApplicationEmitter::writeControlPacket(unsigned char controlField)
{
	// Calculate filename size:
	const char* filename = Application::getFilename();
	size_t filenameSize = strlen(filename) + 1;

	// Allocate memory packet:
	unsigned int packetSize = 9 + filenameSize;
	unsigned char* controlPacket = (unsigned char*) malloc(packetSize);

	// Control byte:
	controlPacket[0] = controlField;
	
	// File Size parameter:
	controlPacket[1] = s_T_FILE_SIZE;
	controlPacket[2] = sizeof(int);
	memcpy(controlPacket+3, &m_fileSize, sizeof(int));

	// Filename parameter:
	controlPacket[7] = s_T_FILENAME;
	controlPacket[8] = filenameSize;
	memcpy(controlPacket + 9, filename, filenameSize);

	// Write packet:
	m_emitter.llwrite(controlPacket, packetSize);

	// Free control packet memory:
	free(controlPacket);
}
void ApplicationEmitter::writeData()
{
	unsigned char sequenceNumber = 0x00;
	
	// Allocate memory for data fragment:
	unsigned char* fragment = (unsigned char*) malloc(m_maxFragmentSize);
	
	// Open file:
	const char* filename = Application::getFilename();
	int file = open(filename, O_RDONLY);
	
	// While not on end-of-file:
	while(true)
	{
		// Read data from file: 
		int fragmentSize = read(file, fragment, m_maxFragmentSize);
		
		// If on end-of-file, end the cycle:
		if(fragmentSize == 0)
			break;
	
		// Write data fragment and increment sequence number:
		writeDataFragment(fragment, fragmentSize, sequenceNumber++);
		
		if (m_emitter.isTimedOut()) break;
	}
	
	// Close file:
	close(file);
	
	// Free data fragment memory:
	free(fragment);
}
void ApplicationEmitter::writeDataFragment(const unsigned char* fragment, unsigned short fragmentSize, unsigned char sequenceNumber)
{
	// Calculate packet size:
	unsigned int packetSize = 4 + fragmentSize;

	// Allocate memory for packet:
	unsigned char* dataPacket = (unsigned char*) malloc(packetSize);

	// Data packet header:
	dataPacket[0] = s_C_DATA;
	dataPacket[1] = sequenceNumber;
	dataPacket[2] = (fragmentSize >> 8) & 0xFF;
	dataPacket[3] = fragmentSize & 0xFF;
	//memcpy(dataPacket + 2, &fragmentSize, sizeof(unsigned short));

	// Data field:
	memcpy(dataPacket + 4, fragment, fragmentSize);

	// Write packet:
	m_emitter.llwrite(dataPacket, packetSize);

	// Free packet memory:
	free(dataPacket);
}

int ApplicationEmitter::calculateFileSize(const char* filename)
{
	// Open file:
	FILE* file = fopen(filename, "r");

	// Find out file size:
	fseek(file, 0L, SEEK_END);
	int size = ftell(file);

	// Close file:
	fclose(file);

	return size;
}

void ApplicationEmitter::printStats() {
		printf("Packets sent: %i\n", m_emitter.m_sentPackets);
		printf("Packets rejected: %i\n", m_emitter.m_rejectedPackets);
		printf("Nº of timeouts: %i\n", m_emitter.m_numTimeOuts);
}

void ApplicationEmitter::setMaxRetransmissions(unsigned int maxRetransmissions)
{
	m_emitter.setMaxRetransmissions(maxRetransmissions);
}
void ApplicationEmitter::setBaudrate(unsigned int baudrate)
{
	m_emitter.setBaurate(baudrate);
}
void ApplicationEmitter::setTimeout(unsigned int timeout)
{
	m_emitter.setTimeout(timeout);
}