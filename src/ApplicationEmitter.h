#ifndef __APPLICATION_EMITTER_H__
#define __APPLICATION_EMITTER_H__

#include "Application.h"
#include "Emitter.h"

class ApplicationEmitter : public Application
{
public:
	ApplicationEmitter();

	bool alopen(const char* com, const char* filename);
	void alclose();
	void alwrite();	
	
	void printStats();

	void setMaxRetransmissions(unsigned int maxRetransmissions);
	void setBaudrate(unsigned int baudrate);
	void setTimeout(unsigned int timeout);
		
private:
	void writeControlPacket(unsigned char controlField);
	void writeData();
	void writeDataFragment(const unsigned char* data, unsigned short dataLength, unsigned char sequenceNumber);

	static int calculateFileSize(const char* filename);

private:
	Emitter m_emitter;
};

#endif
