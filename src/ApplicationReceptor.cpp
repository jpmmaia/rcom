#include "ApplicationReceptor.h"

ApplicationReceptor::ApplicationReceptor() : Application(Application::receptor)
{
	m_startReceived = false;
	m_endReceived = false;
	m_outputFile = NULL;
}
ApplicationReceptor::~ApplicationReceptor()
{

}

bool ApplicationReceptor::alopen(const char* com)
{
	m_startReceived = false;
	m_endReceived = false;
	
	// Setting serial port:
	Application::m_com = com;

	// Open connection with serial port:
	fprintf(stderr, "2.1 - Opening connection with device %s!\n", com);
	Application::m_fd = open(com, O_RDWR | O_NOCTTY);

	// Link layer open connection:
	return m_receptor.llopen(Application::m_fd);
}
void ApplicationReceptor::alclose()
{
	// Close output file
	if (m_outputFile != NULL) {
		fclose(m_outputFile);
		m_outputFile = NULL;
	}
	
	// Link layer close connection:
	m_receptor.llclose();

	// Close connection with device:
	fprintf(stderr, "2.6 - Close connection with device %s!\n", m_com);
	close(Application::m_fd);
}
void ApplicationReceptor::alread()
{
	m_startReceived = false;
	m_endReceived = false;
	m_sequenceNumber = 0;

	unsigned char* packet = (unsigned char*) malloc(m_maxPacketSize+1);
	m_receptor.setMaxFrameSize(m_maxPacketSize+1);
	
	while(!m_endReceived)
	{
		// Read data:
		if(!m_receptor.llread(packet))
		{
			printf("2.Error - while reading start packet!\n");
			return;
		}
		
		//fprintf(stderr, "2.4 - Packet %i received!\n", m_sequenceNumber);
		
		if(packet[0] == s_C_START)
			readControlPacket(packet);
		else if(packet[0] == s_C_DATA)
			readDataPacket(packet);
		else if(packet[0] == s_C_END)
			m_endReceived = true;
	}

	fprintf(stderr, "2.5 - End Control Packet received!\n");

	free(packet);
	fprintf(stderr, "2.6 - Packet free\n");
}


void ApplicationReceptor::readControlPacket(const unsigned char* controlPacket)
{
	const unsigned char* parameter = &controlPacket[1];
	
	// Get parameter type and length:
	unsigned char type = parameter[0];
	unsigned char length = parameter[1];
	
	// Process file size:
	if(type == s_T_FILE_SIZE)
		readFileSize(parameter);
		
	// Process filename:
	else if(type == s_T_FILENAME)
		Application::setFilename((const char*) &parameter[2]);
			
	// Go to next parameter:
	parameter = &parameter[2 + length];
	

	if(controlPacket[0] == s_C_START)
	{
		fprintf(stderr, "2.3 - Start Control Packet received!\n");
		m_sequenceNumber = 0;
		m_startReceived = true;
		
		// Write data to a file with filename "*original-filename* - received":
        fprintf(stderr, "2.3.1 - Opening output file!\n");
		const char* originalFilename = (const char*) &parameter[2];
        
		m_outputFile = fopen(originalFilename, "w");
		if (m_outputFile==NULL) {
				fprintf(stderr, "2.3.2 - Error opening output file!\n");
				exit(-1);
		}
	}
}
void ApplicationReceptor::readFileSize(const unsigned char* fileSizeParameter)
{
	// Set file size:
	m_fileSize = 0;
	memcpy(&m_fileSize, fileSizeParameter+2, sizeof(int));
	fprintf(stderr, "File size ---- %d\n", m_fileSize);
}


void ApplicationReceptor::readDataPacket(const unsigned char* dataPacket)
{
	// If start byte not yet received, then ignore:
	if(!m_startReceived)
		return;

	// If packet out of sequence, then ignore:
	if(dataPacket[1] != m_sequenceNumber)
		return;

	// Calculate packet length:
	unsigned int packetLength = 0;
	packetLength += (dataPacket[2] & 0xFF) << 8;
	packetLength += dataPacket[3] & 0xFF;
	//fprintf(stderr, "2.. packetLength: %u\n", packetLength);

	
	fwrite(dataPacket+4, 1, packetLength, m_outputFile);

	m_sequenceNumber++;	
}


void ApplicationReceptor::printStats() {
		printf("Packets recieved: %i\n", m_receptor.m_recievedPackets);
		printf("Packets rejected: %i\n", m_receptor.m_rejectedPackets);
		printf("Packets repeated: %i\n", m_receptor.m_repeatedPackets);
}

void ApplicationReceptor::setBaudrate(unsigned int baudrate)
{
	m_receptor.setBaurate(baudrate);
}
void ApplicationReceptor::setTimeout(unsigned int timeout)
{
	m_receptor.setTimeout(timeout);
}
