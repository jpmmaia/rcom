#ifndef __APPLICATION_RECEPTOR_H__
#define __APPLICATION_RECEPTOR_H__

#include "Application.h"
#include "Receptor.h"

class ApplicationReceptor : public Application
{
public:
	ApplicationReceptor();
	~ApplicationReceptor();
	
	bool alopen(const char* com);
	void alclose();
	void alread();

	bool openOutputFile(const char* filename);
	bool closeOutputFile();
	int getOutputFile() const;
	
	void printStats();

	void setBaudrate(unsigned int baudrate);
	void setTimeout(unsigned int timeout);
	
private:
	void readControlPacket(const unsigned char* controlPacket);
	void readFileSize(const unsigned char* fileSizeParameter);

	void readDataPacket(const unsigned char* dataPacket);

	void createDataBuffer(unsigned int size);
	void deleteDataBuffer();
	
	Receptor m_receptor;
	bool m_startReceived;
	bool m_endReceived;
	unsigned char m_sequenceNumber;

	FILE* m_outputFile;
};

#endif
