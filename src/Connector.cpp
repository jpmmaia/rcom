#include "Connector.h"


bool g_timeout = true;
void timeoutAlarmHandler(int sig)
{
	if(!g_timeout)
	{
		g_timeout = true;
		printf("3.Alarm - Connection timed out!\n");
	}
}

Connector::Connector()
{
	m_baudrate = B38400;
	m_timeout = 3;
	m_currentFrame = 0;
	m_error = false;
	
	m_timedOut = false;
	
	// Setup SET command:
	m_set[0] = F;
	m_set[1] = A;
	m_set[2] = C_SET;
	m_set[3] = m_set[1] ^ m_set[2];
	m_set[4] = F;

	// Setup DISC command:
	m_disc[0] = F;
	m_disc[1] = A;
	m_disc[2] = C_DISC;
	m_disc[3] = m_disc[1] ^ m_disc[2];
	m_disc[4] = F;

	// Setup UA command:
	m_ua[0] = F;
	m_ua[1] = A;
	m_ua[2] = C_UA;
	m_ua[3] = m_ua[1] ^ m_ua[2];
	m_ua[4] = F;

	for(int i = 0; i < 2; i++) 
	{
		// Setup RR command:
		m_rr[i][0] = F;
		m_rr[i][1] = A;
		m_rr[i][2] = C_RR(i);
		m_rr[i][3] = m_rr[i][1] ^ m_rr[i][2];
		m_rr[i][4] = F;

		// Setup REJ command:
		m_rej[i][0] = F;
		m_rej[i][1] = A;
		m_rej[i][2] = C_REJ(i);
		m_rej[i][3] = m_rej[i][1] ^ m_rej[i][2];
		m_rej[i][4] = F;
	}

	// Stuffing
	m_flag = F;
	m_escape = ESCAPE;
	m_stuffing_flag = m_flag^0x20;
	m_stuffing_escape = m_escape^0x20;
	
	
	// Setup signal:
	signal(SIGALRM, timeoutAlarmHandler);
}

bool Connector::initialize(int fd)
{
	this->m_fd = fd;
	
	if (tcgetattr(m_fd, &m_oldtio) == -1)
	{ /* save current port settings */
		perror("tcgetattr");
		return false;
	}

	struct termios newtio;
	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag = m_baudrate | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;
	newtio.c_lflag = 0;				// set input mode (non-canonical, no echo,...)

	// VTIME e VMIN devem ser alterados de forma a proteger com um temporizador a 
	//leitura do(s) próximo(s) caracter(es)
	newtio.c_cc[VTIME] = 1;   	// inter-character timer unused
	newtio.c_cc[VMIN] = 0;   	// blocking read until X chars received

	// Flush:
	tcflush(m_fd, TCIOFLUSH);

	// Set configuration:
	if (tcsetattr(m_fd, TCSANOW, &newtio) == -1)
	{
		perror("tcsetattr");
		return false;
	}

	return true;
}
bool Connector::shutdown()
{
	//Espera que toda a informação seja enviada
	tcdrain(m_fd);
	
	// Restore old configuration:
	if (tcsetattr(m_fd, TCSANOW, &m_oldtio) == -1)
	{
		perror("tcsetattr");
		return false;
	}

	return true;
}

bool Connector::readCommand(unsigned char* command, int commandSize)
{
	unsigned char c; int state = 0;
	
	// Setup alarm:
	g_timeout = false;
	if (m_timeout > 0) 
		alarm(m_timeout);

	while(!g_timeout)
	{
		// Read char:
		if (read(m_fd, &c, 1) != 1) continue;
		//fprintf(stderr, "readCommand: %#X\n", c);
		
		// Change state:
		if(c == command[state]) state++;
		else if (c == command[0]) state = 1;
		else state = 0;		

		// Check new state:
		if (state >= commandSize) {
			alarm(0);
			return true;			
		}
	}

	return false;
};

void Connector::setBaurate(unsigned int baudrate)
{
	m_baudrate = baudrate;
}
void Connector::setTimeout(unsigned int timeout)
{
	m_timeout = timeout;
}

bool Connector::isTimedOut() const
{
	return m_timedOut;
}
