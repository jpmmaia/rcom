#ifndef __CONNECTOR_H__
#define __CONNECTOR_H__

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#define BIT(N)		1 << N

#define F			0x7E
#define A			0x03
#define C_SET		0x03				// Set up
#define C_DISC		0x0B				// Disconnect
#define C_UA		0x07				// Unnumbered acknowledgement
#define C_RR(R)		0x05 | (R << 7)		// Receiver ready / positive ACK
#define C_REJ(R)	0x01 | (R << 7)	 	// Reject / negative ACK
#define ESCAPE		0x7D

#define CN0 0x00
#define CN1 0x40

class Connector
{
	
protected:
	bool initialize(int fd);
	bool shutdown();
	
	bool readCommand(unsigned char* command, int commandSize);

	int m_fd;
	bool m_error;
	termios m_oldtio;
	speed_t m_baudrate;
	unsigned int m_timeout;
	unsigned int m_currentFrame;
	void increment_currentFrame() {m_currentFrame = m_currentFrame ? 0 : 1; }
	unsigned int m_maxFrameSize;
	
	unsigned char m_set[5];
	unsigned char m_disc[5];
	unsigned char m_ua[5];
	unsigned char m_rr[2][5];
	unsigned char m_rej[2][5];
	
	unsigned char m_flag;
	unsigned char m_escape;
	unsigned char m_stuffing_flag;
	unsigned char m_stuffing_escape;
	bool m_timedOut;
	
	
	public:
	Connector();

	void setBaurate(unsigned int baudrate);
	void setTimeout(unsigned int timeout);

	bool isTimedOut() const;
	
	unsigned int getMaxFrameSize() { return m_maxFrameSize; }
	void setMaxFrameSize(unsigned int maxFrameSize) { m_maxFrameSize = maxFrameSize; }
	
};

#endif
