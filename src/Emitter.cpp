﻿#include "Emitter.h"
#include "Connector.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


Emitter::Emitter()
{
	m_maxRetransmissions = 3;
	m_currentFrame = 0;
}

bool Emitter::llopen(int fd)
{
	fprintf(stderr, "3.1 - Initializing!\n");
	if (!initialize(fd))
		return false;
		
	m_numTimeOuts = 0;
	m_sentPackets = 0;
	m_rejectedPackets = 0;

	fprintf(stderr, "3.2 - Sending SET and wait for UA!\n");
	unsigned int tries = 0;
	bool success = false;
	m_timeout = 3;
	while (!success && tries < m_maxRetransmissions)
	{
		// Send SET:
		write(m_fd, m_set, 5);

		success = readCommand(m_ua, 5);
		tries++; 
	}

	m_error = !success;
	return success;
}
bool Emitter::llclose()
{
	if(!m_error && !m_timedOut)
	{
		fprintf(stderr, "3.3 - Sending DISC and wait for DISC!\n");

		// Send DISC request:
		write(m_fd, m_disc, 5);

		// Wait for confirmation:
		unsigned int tries = 0;
		while (!readCommand(m_disc, 5) && tries < m_maxRetransmissions)
		{
			// Try again:
			fprintf(stderr, "3.3.1 - Resending DISC and wait for DISC! try: %i\n",tries+1);
			write(m_fd, m_disc, 5);
			tries++;
		}
		m_timedOut = tries >= m_maxRetransmissions; 

		if(!m_timedOut) {
			fprintf(stderr, "3.4 - Sending UA!\n");
			// Send final UA:
			write(m_fd, m_ua, 5);
		}
	}

	fprintf(stderr, "3.5 - Shutting down!\n");
	if (!shutdown())
		return false;

	return true;
}

int Emitter::llwrite(const unsigned char* packet, unsigned int packetLength)
{
	// Creating frame buffer:
	unsigned int frameSize = packetLength + 6;
	unsigned char* frame = (unsigned char*) malloc(frameSize);

	createFrame(frame, packet, packetLength);
	
	unsigned int tries = 0;
	bool repeat = true;
	while (repeat && tries<m_maxRetransmissions)
	{
		// Send frame:
		writeFrame(frame, frameSize);

		// Read response. If response is incorrect or timeout occurs, send the frame again:
		repeat = !readResponse();
		tries++;
	}
	m_timedOut = tries>=m_maxRetransmissions;
	
	// Free frame buffer memory:
	free(frame);
	
	if (m_timedOut) 
		return -1;
	else 
		return (int) packetLength;
}





void Emitter::createFrame(unsigned char* frame, const unsigned char* packet, unsigned int packetLength)
{
	unsigned int frameSize = packetLength + 6;
	
	// Creating header:
	frame[0] = F;								// Flag
	frame[1] = A;								// Address
	frame[2] = m_currentFrame ? CN1 : CN0;		// Control
	frame[3] = frame[1] ^ frame[2];				// BCC1

	// Copy packet data to frame:
	memcpy(&frame[4], packet, packetLength);

	// Creating BCC2:
	char bcc2 = 0;
	for (unsigned int i = 0; i < packetLength; i++)
		bcc2 ^= packet[i];

	frame[frameSize - 2] = bcc2;

	// Last flag:
	frame[frameSize - 1] = F;
}
void Emitter::writeFrame(const unsigned char* frame, unsigned int frameLength)
{
	// Write F:
	write(m_fd, &frame[0], 1);

	// Stuffing and writing:
	for (unsigned int i = 1; i < frameLength - 1; i++)
	{
		char word = frame[i];

		// If byte == flag -> 0x7d 0x7E^0x20
		if (word == m_flag)
		{
			write(m_fd, &m_escape, 1);
			write(m_fd, &m_stuffing_flag, 1);
		}
		// If byte == escape character -> 0x7D 0x7D^0x20
		else if (word == m_escape)
		{
			write(m_fd, &m_escape, 1);
			write(m_fd, &m_stuffing_escape, 1);
		}
		// Write byte
		else
			write(m_fd, &word, 1);
	}

	// Write last F:
	write(m_fd, &frame[frameLength - 1], 1);
	
}

extern bool g_timeout;
bool Emitter::readResponse()
{
	// Find out which RR or REJ should we expect:
	unsigned char* rr = m_rr[m_currentFrame];
	unsigned char* rej = m_rej[m_currentFrame];
	//fprintf(stderr, "Expect %#X %#X %#X %#X %#X\n", rr[0], rr[1], rr[2], rr[3], rr[4]);
	
	// Wait for RR or REJ
	ResponseState state = NONE;
	unsigned char c;
	
	// Setup alarm
	g_timeout = false;
	if (m_timeout > 0) 
		alarm(m_timeout);
		
	while(!g_timeout) {
		int r = read(m_fd, &c, 1);
		if (r != 1) continue;
		
		if (state == NONE){
			if (c == F) state = FH_S;
		}else 
		if (state == FH_S) {
			if (c == A) state = A_S;
			else if (c == F) state = FH_S;
			else state = NONE;
		} else
		if (state == A_S) {
			if (c == rr[2]) state = C_RR_S;
			else 
			if (c == rej[2]) state = C_REJ_S;
			else if (c == F) state = FH_S;
			else state = NONE;
		} 
		else
			if (state == C_RR_S) {
				if (c == rr[3]) state = BCC_RR_S;
				else if (c == F) state = FH_S;
				else state = NONE;
			} else
			if (state == BCC_RR_S) {
				if (c == F) {
					// received correct RR
					increment_currentFrame();
					m_sentPackets++;
					alarm(0);
					return true;
				}
				else state = NONE;
			} 
		else
			if (state == C_REJ_S) {
				if (c == rej[3]) state = BCC_REJ_S;
				else if (c == F) state = FH_S;
				else state = NONE;
			} else
			if (state == BCC_REJ_S) {
				if (c == F) {
					// received correct REJ
					m_rejectedPackets++;
					alarm(0);
					return false; 
				}
				else state = NONE;
			}
			
	}

	// Timeout
	m_numTimeOuts++;
	return false;
}

void Emitter::setMaxRetransmissions(unsigned int maxRetransmissions)
{
	m_maxRetransmissions = maxRetransmissions;
}
