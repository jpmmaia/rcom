#ifndef __EMITTER_H__
#define __EMITTER_H__

#include "Connector.h"

class Emitter : public Connector
{
public:
	Emitter();
	
	bool llopen(int fd);
	int llwrite(const unsigned char* packet, unsigned int packetLength);
	bool llclose();

	void setMaxRetransmissions(unsigned int maxRetransmissions);
	
	unsigned int m_numTimeOuts;
	unsigned int m_sentPackets;
	unsigned int m_rejectedPackets;
	
private:
	void createFrame(unsigned char* frame, const unsigned char* packet, unsigned int packetLength);
	void writeFrame(const unsigned char* frame, unsigned int frameLength);
	bool readResponse();
	
	enum ResponseState {
		NONE = 0,
		FH_S = 1,
		A_S = 2,
		C_RR_S = 3,
		C_REJ_S = 4,
		BCC_RR_S = 5,
		BCC_REJ_S = 6
	};
	
	unsigned int m_maxRetransmissions;
};

#endif
