#include "Receptor.h"



Receptor::Receptor() {
}


bool Receptor::llopen(int fd) {

    // Initialize:
	fprintf(stderr, "3.1 - Initializing!\n");
	if (!initialize(fd))
		return false;
		
	m_recievedPackets = 0;
	m_rejectedPackets = 0;
	m_repeatedPackets = 0;

	// Disabling alarm:
	m_timeout = 0; 

	// Waiting for set:
	fprintf(stderr, "3.2 - Waiting for SET!\n");
	while(!readCommand(m_set, 5));

	// Reply
	fprintf(stderr, "3.3 - Sending UA!\n");
	write(m_fd, m_ua, 5);

	return true;
};


bool Receptor::llclose() { 

	if(!m_error)
	{
		// Wait for disc:
		fprintf(stderr, "3.4 - Waitting for DISC!\n");
		while (!readCommand(m_disc, 5));

    	// Send DISC back:
		fprintf(stderr, "3.5 - Sending DISC!\n");
		write(m_fd, m_disc, 5);

    	// Wait for UA:
		fprintf(stderr, "3.6 - Waitting for UA!\n");
		while (!readCommand(m_ua, 5))
		{
			// Try send disc again:
			write(m_fd, m_disc, 5);
		}
	}
	
	fprintf(stderr, "3.7 - Shuttting down!\n");
	if (!shutdown())
		return false;

	return true;
};

#define aux_writeData(c) buffer[dataIndex++] = (c); BCC2XOR^= (c);

int Receptor::llread(unsigned char* buffer){
		
	unsigned char c;  
	unsigned int dataIndex = 0;
	unsigned char BCC2XOR = 0;
	
	ReadState state = NONE;
	unsigned char CNt = m_currentFrame%2 == 0 ? CN0 : CN1;
	unsigned char CNo = CNt == CN0 ? CN1 : CN0;
	unsigned char Bcc1 = A^CNt;
	unsigned int maxFrameSize = getMaxFrameSize();
	
	while(true) {
		int r = read(m_fd, &c, 1);
		if (r != 1) continue;
		//printf("r: %.2x %c, state = %i, di = %i\n",c,c,state, dataIndex);

		// READ DATA
		if (state == DATA_S) {
			if (c == m_escape) state = DESTUFFING_S;
			else if (c == F) {
				// final do processamento da trama
				if (BCC2XOR == 0) {
					// trama sem erros
					// Send RR and carry on to next frame
					write(m_fd, m_rr[m_currentFrame], 5);	
					m_recievedPackets++;
					increment_currentFrame();
					return dataIndex-1;
				} else {
					// trama com erros
					// enviar rej
					write(m_fd, m_rej[m_currentFrame], 5);
					m_rejectedPackets++;
					
					state = NONE;
				}
			} else {
				if (dataIndex >= maxFrameSize) {
					// trama com erros ( tamanho de packet excedido )
					// enviar rej
					write(m_fd, m_rej[m_currentFrame], 5);
					m_rejectedPackets++;
					
					state = NONE;
				}
				else
					aux_writeData(c);
				
			}
		} 
		else if (state == DESTUFFING_S) {
			if (c == m_stuffing_flag) { aux_writeData(m_flag); }
			else if (c == m_stuffing_escape) { aux_writeData(m_escape); }

			state = DATA_S;
		} 
		
		// IGNORE REPEATED FRAME
		else if (state == IGNORE_S) {
			if (c == F) {
				// acknoledge repeated frame
				printf("3.8 - Recieved repeated frame \n");
				m_repeatedPackets++;
				write(m_fd, m_rr[m_currentFrame ?  0 : 1], 5);
				state = NONE;
				dataIndex = 0;
			}
		}
		
		
		// READ HEADER
		else if(state == NONE) {
			if (c == F)
				state = FH_S;		
		}
		else if (state == FH_S) {
			if(c == A) state = A_S;
			else if (c == F) state = FH_S;
			else state = NONE;
		} 
		// 		read packet type
		else if (state == A_S) {
			if(c == CNt ) state = CNT_INFO_S; // correct data packet
			else if (c == CNo) state = IGNORE_S; // repeated data packet
			else if(c == m_set[2]) state = CNT_SET_S; // repeated set command
			else if (c == F) state = FH_S;
			else state = NONE;
		} 
		else if (state == CNT_INFO_S) {
			if (c == Bcc1 ) {
				// start data input
				state = DATA_S;
				BCC2XOR = 0;
				dataIndex = 0;
			}
			else if (c == F) state = FH_S;
			else state = NONE;
		}
		
		// READ REPEATED SET
		else if (state == CNT_SET_S) {
			if (c == m_set[3]) state = BCC_SET_S;
			else if (c == F) state = FH_S;
			else state = NONE;
		}
		else if (state == BCC_SET_S) {
			if (c ==F) {
				// Reply to SET with UA
				write(m_fd, m_ua, 5);
			}
			state = NONE;
		}
		
		// DEFAULT	
		else {
			state = NONE;
		}
	};	
	
	
}






