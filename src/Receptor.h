#ifndef __RECEPTOR_H_
#define __RECEPTOR_H_

#include "Connector.h"


class Receptor : public Connector
{
public:

	Receptor();
	~Receptor(){};
	
	
	bool llopen(int fd);
	int llread(unsigned char* packet);
	bool llclose();
	
	unsigned int m_recievedPackets;
	unsigned int m_rejectedPackets;
	unsigned int m_repeatedPackets;

	
private:
	enum ReadState 
	{ 
		NONE = 0, 
		FH_S = 1, 
		A_S = 2, 
		
		CNT_INFO_S = 3, 
		DATA_S = 4,
		DESTUFFING_S = 5, 
		IGNORE_S = 6,
		
		CNT_SET_S = 7,  
		BCC_SET_S = 8, 
		
		FT_S = 9, 		
	};		

};


#endif
