#include "ApplicationReceptor.h"
#include "ApplicationEmitter.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

void registerHandler();
void segmentationFaultHandler(int signum, siginfo_t *info, void *context);

speed_t getBaudrate(int br);

void printUsage(const char* argv[])
{
	printf("Parameters:\n");
    printf("\t1st - (string) serialPort\n");
    printf("\t2nd - 0 for receiver, 1 for emitter\n");
    printf("\t3rd - [emitter only] (string) filename\n");
    printf("\n");
    printf("Optional parameters:\n");
    printf("\t-baudrate value (uint)\n"); 
    printf("\t-infosize maxInfoFieldSize (uint)\n");
    printf("\t-tries [emitter only] maxRetransmissions (uint)\n");
}

int processOptionalParameter(int argc, const char* argv[], const char* parameterName)
{
    for(int i = 3; i < argc - 1; i++)
    {
        if(strcmp(argv[i], parameterName) == 0) {
				return atoi(argv[i + 1]);
        }
    }

    int defaultValue = 0;
    if(strcmp(parameterName, "-baudrate") == 0)
        defaultValue = 38400;
    else if(strcmp(parameterName, "-infosize") == 0)
        defaultValue = 512;
    else if(strcmp(parameterName, "-tries") == 0)
        defaultValue = 3;

    return defaultValue;
}

void setOptionalParameters(int argc, const char* argv[], Application* app)
{
    int baudrate = processOptionalParameter(argc, argv, "-baudrate");
    app->setBaudrate(getBaudrate(baudrate));
    fprintf(stderr, "0 - Parameter baudrate = %d\n", baudrate);

    int infoSize = processOptionalParameter(argc, argv, "-infosize");
    app->setMaxFragmentSize(infoSize);
    fprintf(stderr, "0 - Parameter info size = %d\n", infoSize);
}

int main(int argc, const char* argv[])
{ 	
	// Check if num parameters is at least 3:
	if(argc < 3)
	{
		printUsage(argv);
      	exit(-1);
	}
	
	// Get serial port:
	const char* serialPort = argv[1];

	// Checking if it is emitter or receiver: (receiver == 0, emitter == 1):
	bool emitter = (strcmp(argv[2], "1") == 0);

    // Register handler for segmentation fault (debugging purposes):
    registerHandler();

    bool success = false;
    if(emitter)
    {

		// Checking if there is 4 parameters:
		if(emitter && argc < 4)
		{
			printUsage(argv);
			exit(-1);
		}

		// Get filename:
    	const char* filename = argv[3];

    	// Creatting emitter app:
    	ApplicationEmitter app;

        // Set max number of retransmissions:
        int tries = processOptionalParameter(argc, argv, "-tries");
        app.setMaxRetransmissions(tries);
        fprintf(stderr, "0 - Parameter tries = %d\n", tries);

        // Set other optional parameters:
        setOptionalParameters(argc, argv, &app);

        // Open connection:
        fprintf(stderr, "1.1 - Opening application connection!\n");
    	success = app.alopen(serialPort, filename);
        
        if(success)
        {
            // Send file data:
            fprintf(stderr, "1.2 - Send file data!\n");
            app.alwrite();
            
            // Print stats
            printf("\n");
            app.printStats();
        }
    	
    	// Close connection:
        fprintf(stderr, "1.3 - Closing application connection!\n");
    	app.alclose();
    }
    else
    {
    	// Creating receptor app:
    	ApplicationReceptor app;

        // Set other optional parameters:
        setOptionalParameters(argc, argv, &app);

        // Open connection:
        fprintf(stderr, "1.1 - Opening application connection!\n");
    	success = app.alopen(serialPort);

        if(success)
        {          
            // Read data:
            fprintf(stderr, "1.2 - Reading data!\n");
            app.alread();
            
            // Print stats
            printf("\n");
            app.printStats();

        }        

    	// Close connection:
        fprintf(stderr, "1.3 - Closing application connection!\n");
    	app.alclose();
    }
		
	return 0;
}

void registerHandler()
{
    struct sigaction action;
    action.sa_sigaction = segmentationFaultHandler;
    action.sa_flags = SA_SIGINFO;


    if (sigaction(SIGSEGV, &action, NULL) < 0)
        perror("sigaction");
}

void segmentationFaultHandler(int signum, siginfo_t *info, void *context)
{
    struct sigaction action;
    action.sa_handler = SIG_DFL;

    fprintf(stderr, "Fault address: %p\n", info->si_addr);
    switch (info->si_code) 
    {
    case SEGV_MAPERR:
        fprintf(stderr, "Address not mapped.\n");
        break;

    case SEGV_ACCERR:
        fprintf(stderr, "Access to this address is not allowed.\n");
        break;

    default:
        fprintf(stderr, "Unknown reason.\n");
        break;
    }

    // unregister and let the default action occur:
    sigaction(SIGSEGV, &action, NULL);
}

speed_t getBaudrate(int br){
	
	if (br == 0) return B0;
	if (br == 50) return B50;
	if (br == 75) return B75;
	if (br == 110) return B110;
	if (br == 134) return B134;
	if (br == 200) return B200;
	if (br == 300) return B300;
	if (br == 600) return B600;
	if (br == 1200) return B1200;
	if (br == 1800) return B1800;
	if (br == 2400) return B2400;
	if (br == 4800) return B4800;
	if (br == 9600) return B9600;
	if (br == 19200) return B19200;
	if (br == 38400) return B38400;
	
	return B38400;
}

